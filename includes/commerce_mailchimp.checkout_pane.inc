<?php

/**
 * @file
 * Commerce Mailchimp module.
 */

//module_load_include('inc', 'mailchimp_lists', 'mailchimp_lists.entity');

/**
 * Implements base_settings_form()
 */
function commerce_mailchimp_pane_settings_form($checkout_pane) {
  $form['commerce_mailchimp'] = array(
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
    '#title' => t('Mailchimp commerce'),
  );

  $form['commerce_mailchimp']['commerce_mailchimp_checkout'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Show subscription options at Commerce checkout'),
    '#description'    => t('If set, the user will be presented with the option to subscribe to your list when buying a product'),
    '#default_value' => variable_get('commerce_mailchimp_checkout', ''),
  );
  $form['commerce_mailchimp']['commerce_mailchimp_checkout_description'] = array(
    '#type' => 'textarea',
    '#title'          => t('Description in Commerce checkout'),
    '#description'    => t('A description for the user that shows next to the subscription option in Commere checkout'),
    '#default_value' => variable_get('commerce_mailchimp_checkout_description', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="commerce_mailchimp_checkout"]' => array('checked' => TRUE),
      ),
    ),
  );
  $form['commerce_mailchimp']['commerce_mailchimp_checkout_defaultvalue'] = array(
    '#type'           => 'checkbox',
    '#title'          => t('Subscription option checked by default'),
    '#description'    => t('The default subscription option that is show to the user'),
    '#default_value' => variable_get('commerce_mailchimp_checkout_defaultvalue', ''),
    '#states' => array(
      'visible' => array(
        ':input[name="commerce_mailchimp_checkout"]' => array('checked' => TRUE),
      ),
    ),
  );
  
  return $form;
}

/**
 * Implements base_checkout_form()
 */
function commerce_mailchimp_pane_checkout_form($form, $form_state, $checkout_pane, $order) {
  if (variable_get('commerce_mailchimp_checkout', array())) {
    $checkout_form['commerce_mailchimp'] = array(
      '#type' => 'checkbox',
      '#title' => t('Subscribe me to your newsletter'),
      '#default_value' => variable_get('commerce_mailchimp_checkout_defaultvalue', ''),
      '#description' => filter_xss(variable_get('commerce_mailchimp_checkout_description', '')),
    );
   
  }
  return $checkout_form;
}

/**
 * Implements base_checkout_form_submit()
 */
function commerce_mailchimp_pane_checkout_form_submit($form, &$form_state, $checkout_pane, $order) {
  // do something here with 
  // $form_state['values']['commerce_mailchimp_pane_field2']
}

/**
 * Custom validate handler for checkout forms 
 */ 
function commerce_mailchimp_pane_checkout_form_validate(&$form, &$form_state){
  // validate here
  return true;
}

/**
 * Implements base_review()
 */
function commerce_mailchimp_pane_review($form, &$form_state, $checkout_pane, $order) {
  
  if (variable_get('commerce_mailchimp_checkout', '')) {
    $output['mailchimp_commerce_checkout_decision'] = array(
      '#type' => 'markup',
      '#markup' => 'Subscribe to newsletter: Yes',
    );
  }
  else {
    $output['mailchimp_commerce_checkout_decision'] = array(
      '#type' => 'markup',
      '#markup' => 'Subscribe to newsletter: No',
    );
  }
  return drupal_render($output);
}
